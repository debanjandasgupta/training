const expr = require('express');
const sequelize = require('./db/db');
const product = require('./models/products');


const app = expr();

//Add Function
function add_product(name, desc, price, url) {
    if (product.create({ name: name, description: desc, price: price, image_url: url })) {
        return "1 product added..";
    }
    else {
        return "some error occured";
    }
}
//Add Route
app.get("/add/:prod_name&:prod_desc&:prod_price&:prod_img_url", (req, res) => {
    let name = req.params.prod_name;
    let desc = req.params.prod_desc;
    let price = req.params.prod_price;
    let url = req.params.prod_img_url;
    console.log(name, desc, price, url);
    let stats = add_product(name, desc, price, url);
    res.send(stats);
});
//Delete Function
function del_product(id) {
    if (product.destroy({ where: { id: id } })) {
        return "1 product deleted..";
    }
    else {
        return "some error occured";
    }
}
//Delete Route
app.get("/delete/:prod_id", (req, res) => {
    let prod = req.params.prod_id;
    let stats = del_product(prod);
    res.send(stats);
});
//Name Update Function
function update_product_name(id, name) {
    if (product.update({ name: name }, { where: { id: id } })) {
        return "1 product updated..";
    }
    else {
        return "some error occured";
    }
}
//Name Update Route
app.get("/update/:id/name/:prod_name", (req, res) => {
    let prod = req.params.id;
    let name = req.params.prod_name;
    let stats = update_product_name(prod, name);
    res.send(stats);
});
//Description Update Function
function update_product_description(id, desc) {
    if (product.update({ description: desc }, { where: { id: id } })) {
        return "1 product updated..";
    }
    else {
        return "some error occured";
    }
}
//Description Update Route
app.get("/update/:id/desc/:prod_desc", (req, res) => {
    let prod = req.params.id;
    let desc = req.params.prod_desc;
    let stats = update_product_description(prod, desc);
    res.send(stats);
});
//Price Update Function
function update_product_price(id, price) {
    if (product.update({ price: price }, { where: { id: id } })) {
        return "1 product updated..";
    }
    else {
        return "some error occured";
    }
}
//Price Update Route
app.get("/update/:id/price/:prod_price", (req, res) => {
    let prod = req.params.id;
    let price = req.params.prod_price;
    let stats = update_product_price(prod, price);
    res.send(stats);
});
//Image URL Update Function
function update_product_image(id, url) {
    if (product.update({ image_url: url }, { where: { id: id } })) {
        return "1 product updated..";
    }
    else {
        return "some error occured";
    }
}
//Price Update Route
app.get("/update/:id/image/:img_url", (req, res) => {
    let prod = req.params.id;
    let url = req.params.img_url;
    let stats = update_product_image(prod, url);
    res.send(stats);
});
//Fetch All Data Function
async function get_product_data_all() {
    let productsData = await product.findAll({ attributes: ['name', 'description', 'price'] });
    let s = productsData.map(products => {
        return {
            Name: products.name,
            Description: products.description,
            Price: products.price
        };
    })
    return s;
}
//Fetch All Data Route
app.get("/show", async (req, res) => {
    let stats = await get_product_data_all();
    res.send(stats);
});
//Fetch one Data Function
function get_product_data_one(prodid, res) {
    product.findOne({
        where: { id: prodid },
        attributes: ['name', 'description', 'price']
    }).then((products) => {
        let m = {
            Name: products.name,
            Description: products.description,
            Price: products.price
        }
        res.send(JSON.stringify(m));
    });
}
//Fetch All Data Route
app.get("/show/:id", (req, res) => {
    get_product_data_one(req.params.id, res);
});


const port = process.env.port || 3000;
app.listen(port, () => console.log(`Listening to ${port}...`));